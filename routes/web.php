<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\booksController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/books', booksController::class);
Route::resource('/create', booksController::class);
Route::resource('/edit', booksController::class);

Route::get('/search', [booksController::class, 'search']);


Auth::routes();

Route::get('/',  [booksController::class, 'landing_page']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

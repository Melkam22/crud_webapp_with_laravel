<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;
use \Illuminate\Http\Response;
//use App\Http\Requests\CreateValidationRequest;

class booksController extends Controller
{
    //prevent unallowed entry of routes
    public function _construct(){
        $this->middleware('auth', ['except'=> ['index', 'search']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::all();
        return view('books.index', ['books' => $books]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('books.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

//    CreateValidationRequest
    public function store(Request  $request)
    {
        //$request -> validated();
        $request->validate([
        'title'=> 'required',
         'author'=> 'required',
         'genre'=> 'required',
         'availability'=> 'required',
         'available_language'=>'required',
         'publication_year'=> 'required | integer | min:0 | max: 2022',
         'image'=> 'required | mimes :jpg, png, jpeg | max: 5048'
    ]);

        $newImage =time() . '_' . $request->name . '.' . $request->image->getClientOriginalName();
        $request ->image->move(public_path('images'), $newImage);
        //$test = $request->file('image')->getClientOriginalName();
        //dd( $test);

        $book = Book::create([
            'title' => $request -> input('title'),
            'author' => $request -> input('author'),
            'genre' => $request -> input('genre'),
            'availability' => $request -> input('availability'),
            'available_language' => $request -> input('available_language'),
            'publication_year' => $request -> input('publication_year'),
            'image_path'=> $newImage,
            'user_id'=> auth()->user()->id
      ]);
       return redirect('/books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $book = Book::find($id);
        return view('books.edit')->with('book', $book);
//        $book = Book::select('select * from books where id = ?', [$id]);
//        return view('books.edit', ['book'=>$book]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$request->validated(); //CreateValidationRequest this replaced with Request and $req... commented out

        $book = Book::where('id', $id)
            ->update([
            'title' => $request -> input('title'),
            'author' => $request -> input('author'),
            'genre' => $request -> input('genre'),
            'availability' => $request -> input('availability'),
            'available_language' => $request -> input('available_language'),
            'publication_year' => $request -> input('publication_year'),


]);
        return redirect('/books');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect('/books');
    }
//    search method created
public function search(){
        $searched_name = $_GET['query'];
        $books = Book::where('author', 'LIKE', '%'.$searched_name.'%')->get();
        return view('books.search', compact('books'));
}

//landing page
public function landing_page(){
        return view('books.landing_page');
}
}

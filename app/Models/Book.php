<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;

    protected $table = 'books';
    protected $primaryKey = 'id';
    public $timestamps = true;
    protected $dateFormat = ('h:m:s');
    protected $fillable = [
        'title',
        'author',
        'genre',
        'availability',
        'available_language',
        'publication_year',
        'image_path',
        'user_id'
    ];
}

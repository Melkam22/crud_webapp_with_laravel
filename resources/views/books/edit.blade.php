@extends('layouts.app')

@section('content')
<h1>Update</h1>
{{--<a href="/books">Back</a>--}}

<a href="/books">Back</a><br>
<form action="/books/{{$book->id}}" method="POST">
    @csrf
    @method('PUT')
    <input type="text" name="title" value="{{$book->title}}">
    <input type="text" name="author" value="{{$book->author}}"/>
    <input type="text" name="genre" value="{{$book->genre}}"/>
    <input type="text" name="availability" value="{{$book->availability}}"/>
    <input type="text" name="available_language" value="{{$book->available_language}}"/>
    <input type="number" name="publication_year" value="{{$book->publication_year}}"/>
    <button type="submit">Submit</button>
</form>
@endsection

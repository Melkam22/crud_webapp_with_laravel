@extends('layouts.app')


@section('content')
<h1 class="colorVar" style="margin-left: 20px;">Books list</h1>
{{--create new books--}}

{{--trying the auth method--}}
{{--{{dd(Auth::user())}}--}}

{{--show this button only to logged in users--}}
@if(Auth::user())
    <div class="createNew" style="margin-left: 20px;">
        <a href="books/create">Insert Book</a>
    </div>
@else
    <h2>You have to be logged_in in order to be able to create items.</h2>
@endif


{{--filter with name --}}
<form action="{{url('/search')}}" type="get" style="margin-left: 20px;">
    <input type="text" name="query" placeholder="filter by name of author ...">
    <button class="deleteBtn">Filter</button>
</form>


{{--{{dd($books)}}--}}
@foreach($books as $book)

    {{--image upload--}}
{{--    <img src="{{asset('images/'.$book->image_path)}}" alt="" style="width: 180px; height: 180px; border-radius: 50px">--}}

<h2 style="margin-left: 20px;">{{$book->title}}</h2>
 <h5 style="margin-left: 20px;">{{$book->author}}</h5>
<h5 style="margin-left: 20px;">{{$book->genre}}</h5>
<h5 style="margin-left: 20px;">{{$book->availability}}</h5>
<h5 style="margin-left: 20px;">{{$book->available_language}}</h5>
<h5 style="margin-left: 20px;">{{$book->publication_year}}</h5>

{{--    add edit btn--}}

    {{--dont show edit & delete buttons for non logged users function--}}
    {{--@if (isset(Auth::user()->id) && Auth::user()->id == $book->user_id)--}}
    @if(Auth::user())
        <div style="margin-left: 20px;">
            <a href="/books/{{$book->id}}/edit">Edit</a>
        </div>
        {{--delete btn & function--}}
        <form action="/books/{{$book->id}}" method="POST" style="margin-left: 20px;">
            @csrf
            @method('delete')
            <button class="deleteBtn">Delete</button>
        </form>
    @endif

<hr>
@endforeach
@endsection

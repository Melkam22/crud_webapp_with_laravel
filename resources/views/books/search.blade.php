@extends('layouts.app')


@section('content')
    <h1>Books list</h1>
{{--    go back--}}
    <a href="/books">Back</a><br>

    {{--create new books--}}

{{--    <div class="createNew">--}}
{{--        <a href="books/create">Insert Book</a>--}}
{{--    </div>--}}

    {{--filter with name --}}
    <form action="{{url('/search')}}" type="get">
        <input type="text" name="query" placeholder="filter by name of author ...">
        <button class="deleteBtn">Filter</button>
    </form>

    {{--{{dd($books)}}--}}
    @foreach($books as $book)
        {{--image upload--}}
        <img src="{{asset('images/'.$book->image_path)}}" alt="" style="margin-top: 15px; width: 180px; height: 180px; border-radius: 50px">

        <h2>{{$book->title}}</h2>
        <h5>{{$book->author}}</h5>
        <h5>{{$book->genre}}</h5>
        <h5>{{$book->availability}}</h5>
        <h5>{{$book->available_language}}</h5>
        <h5>{{$book->publication_year}}</h5>

        {{--    add edit btn--}}
        <div>
            <a href="books/{{$book->id}}/edit">Edit</a>
        </div>
        {{--delete btn & function--}}
        <form action="/books/{{$book->id}}" method="POST">
            @csrf
            @method('delete')
            <button class="deleteBtn">Delete</button>
        </form>
        <hr>
    @endforeach
@endsection

@extends('layouts.app')

@section('content')
    {{--I add this to redirect authenticated user to be able to perform CRUD op.--}}
    <nav style="height: 50px; width: 100vw; background: #0c63e4; display: flex">
        <a href="books" style="margin-top: 7px; margin-left: 10px;"> <button style="height: 34px;">Home</button></a>
    </nav>
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
{{--                            What I add to welcome page with user name--}}
                    {{ __('Welcome') }} {{ Auth::user()->name }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
